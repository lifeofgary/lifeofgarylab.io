
# Start Server

- start server `hexo server`
- start server with drafts `hexo seerver --draft`

# Create a post

- create a new post `hexo new <title>`
- create a draft post `hexo new draft <title>
- publish a draft `hexo publish <title>`

# File locations

- post are in `source/_posts/`
- drafts are in `source/_drafts/`

