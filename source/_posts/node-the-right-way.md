---
title: Node the Right Way
date: 2017-12-11 07:25:44
tags: ['Book', 'Javascript', 'Node']
category: ['Books', 'Javascript', 'Node']
stars: 4
difficulty: 4

---

<img src="https://imagery.pragprog.com/products/351/jwnode.jpg?1381944730" alt="Node the Rigth Way" style="width: 25%;"/>

This book is a quick read.  It touches on all the major features of Node to get you started without going in too deeply.  Because the node ecosystem is moving quickly it suffers from using some old features, like promise libraries.  It shows single solutions which can be a disadvantage, as with CouchDb or an advantage as with 0MQ and Express.  It is a quick read and a great place to get started with Node or any topic it covers.

## Outline

1. Getting Started - An overview and how to install node
2. Wrangling the File system
  * Child Process (pg 13)
  * EventEmitter (pg 15)
3. Networking with Sockets
4. Robust Messagin Services
  * 0MQ pg (42)
5. Accessing Databases CouchDb
  * Download Project Gutenberge Data (pg 70)
6. Scalable Web Services
  * Basics of Express.js
7. WebApps
  * Authenticating with Passport (pg 113)
  * Custom Middleware (page 117)

## Chapter 1  - Getting started

An overview of node and how to install it

### Highlights

Online [Resources](http://pragprog.com/book/jsnode/node-js-the-right-way)

## Chapter 2 - The file system

Overview of the file system and nodes asyncronous nature

### Highlights

#### Child Process (pg 13)

```
const fs = require('fs')
const spawn = require('child_process').spawn
const filename = process.arg[2]

if (!filename) throw Error ("A file to watch must be specified!")

fs.watch(filename, ()=>{
  let ls = spawn('ls', ['-lh', filename])
  ls.stdout.pipe(process.stout)
})
console.log("Now watching " + filename + " for changes...")
```

#### EventEmitter (pg 15)

```
const fs = require('fs')
const spawn = require('child_process').spawn
const filename = process.arg[2]

if (!filename) throw Error ("A file to watch must be specified!")

fs.watch(filename, ()=>{
  let s=spawn('ls', ['-lh', filename])
  let output=''

  ls.stdout.on('data', chunk=>output += chunk.toString() )
  ls.on('close', ()=>{
    let parts = output.split(/|s+/)
    console.dir(parts[0], parts[4], parts[8])
  })
})
console.log("Now watching " + filename + " for changes...")
```

## Chapter 3 - Networking with Sockets

Low level overview which is obsoleted by the next chaper

## Chapter 4 - Robust Messagin Services

This is about using the [0MQ](http://www.zeromq.org) library for creating an internale TCP publisher/subscriber pattern.  The whole chapter is useful.

## Chapter 5 - Accessing Databases CouchDb

 A great deal about using CouchDb

### Highlights

#### Download [Project Gutenberge](http://www.gutenberg.org) Data (pg 70)

```
curl -O http://www.gutenberg.org/cache/epub/feeds/red-files.tar.bz2
tar -xvjf rdf-files.tar.bz2
npm install --save cheerio
```

Create a file (pg 72)

```
// rdf-parser.js
const fs = require('fs)
const cheerio = require('cheerio')

module.exports = (filename, callback) => {
  fs.readFile( filename, (err, data)=>{
    if (err) { return callback(err)}
    let $ = cheerio.load(data.toString())
    let collect = (index, elem) => {
      return $(elem).text()
    }

    callback(null, {
      _id: $('pgterms||":ebook').attr('rdf:about).replace('ebooks/, ''),
      title: $('dcterms||:title').text(),
      authors: $('pgterms||:agent pgterms||:name').map(collect),
      subjects: $('[rdf||:resource$="/LCSH"] ~ rdf||:value').map(collect)
    })
  })
}

```

Test the file by

```
node -e 'require("rdf-parser.js")('cache/epub/132/pg132.rdf")
```

## Chapter 6 - Scalable Web Services

This converse [Express.js](www.express.js)

## Chapter 7 - WebApps

This is more about Express

### Highlights

#### Authenticating with Passport (pg 113)

```
// Include
passport = require('passport')
GoogleStrategy = require('passport-google').Strategy

...

// Init
app.use(passport.initialize())
app.use(passport.session())

...

// Configure
passport.serializeUser( (user, done)=>done(null, user.identifier))
passport.deserializeUser( (user, done)=>done(null, {identifier: id}))

...

// Use GoogleStrategy
passport.use(new GoogleStrategy({
  returnURL: 'http://localhost:3000/auth/google/return',
  realm: 'http://localhost:3000/'},
  (id, profile, done) => {
    profile.identifier = id
    return done(null, profile)
  }
))

...

// Add to route
app.get('/auth/google/:return?'), passport.authenticate('google', {successRedirect: '/' } )
app.get('/auth/logout', (req, res)=>{
  req.logout()
  res.redirect('/')
})

```

#### Custom Middleware (page 117)

```
const authed = (req, res, next)=>{
  if (req.isAuthenticated()) return next()
  else return ({
    res.json( 403, {
      error: 'forbidden',
      reason: 'not_authenticated'
    })
  })
}
```
