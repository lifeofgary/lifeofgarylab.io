---
title: redux-quick-start-ramda
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
---

* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)
* [Redux Leaflet](../redux-quick-start-leaflet)
* [Redux Immutable](../redux-quick-start-immutable)

Javascript copies nested object poorly, it does a shallow copy for for anything greater than two levels deep.  For more information see Copying Object in [Javascript](https://scotch.io/bar-talk/copying-objects-in-javascript).  This will cause problems in a reducer.  There are several solutions.  The simplest solution is to never use nested objects in a store, but sometimes nested stores are needed. A popular one is [ImmutableJS](https://facebook.github.io/immutable-js/) but the syntax is noisy.  I like using [Ramada](https://ramdajs.com/) which is a functional programmming library which adds helpful functions.

Here are useful Ramda functions in

* copy a state: [R.clone](https://ramdajs.com/docs/#clone)
* set a value: [R.assocPath](https://ramdajs.com/docs/#assocPath)
* get a value [R.path](https://ramdajs.com/docs/#path)
* delete a key [R.dissocPath](https://ramdajs.com/docs/#dissocPath)

To see these in action

[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/442d18b976eb2faf13eff9efc5537c216aaab4e1)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/442d18b976eb2faf13eff9efc5537c216aaab4e1)
`git checkout 442d18b9`

# Refeneces

* [Thinking in Ramda](http://randycoulman.com/blog/2016/05/24/thinking-in-ramda-getting-started/)
* [Javascript](https://scotch.io/bar-talk/copying-objects-in-javascript)
* [Handling state in React](https://medium.freecodecamp.org/handling-state-in-react-four-immutable-approaches-to-consider-d1f5c00249d5)
* [Functional Redux Reducers with Ramda](https://alligator.io/redux/functional-redux-reducers-with-ramda/)
* [Ramda React Patterns](https://tommmyy.github.io/ramda-react-redux-patterns/)
