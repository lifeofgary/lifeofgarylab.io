---
title: redux-quick-start-semantic
date: 2019-01-05 02:58:18
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
---

This is part of the [React Quick Start](../redux-quick-start) series.  The articles are

* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)
* [Redux Leaflet](../redux-quick-start-leaflet)
* [Redux Immutable](../redux-quick-start-immutable)

```bash
yarn add semantic-ui-react
yarn add semantic-ui-css
```
# Semantic Button

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/a5df45d454546022a3a261ddd8a08f556773e5aa)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/a5df45d454546022a3a261ddd8a08f556773e5aa)
`git checkout a5df45d4`

In `src/index/js` include the semantic css file
```js
// Include semantic ui css from node
import 'semantic-ui-css/semantic.min.css';
```

Now convert the `<button>` to semantic `<Button>` component.  Note lowercase means an html element
and uppercase means a React Component

In `src/container/Container.jsx`
```jsx
import { Button } from 'semantic-ui-react';
...snip...
        <Button onClick={props.setStatelessText}>
            Update Stateless
        </Button>

        <Pure header="Pure" text={pure_text}/>
        <Button onClick={()=>props.setPureText("Update Pure Text.")}>
            Update Pure
        </Button>
...snip...
```

# Semantic Container

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/b01b60c01f1b4b6e36b4bc9a20b911f76d8f5a60)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/b01b60c01f1b4b6e36b4bc9a20b911f76d8f5a60)
`git checkout b01b60c0`

To add some spacing around the page it is nice to wrap it in a containter.

Refactor `src/App.js` to be
```jsx
import React from 'react';
import Container from './container/Container'
import { Container as SemanticContainer } from 'semantic-ui-react';

class App extends React.Component {
  render() {
    return (
      <Container/>
      <SemanticContainer>
        <Container/>
      </SemanticContainer>
    );
  }
}

export default App;
```

# References

* [Semantic-UI-React](https://react.semantic-ui.com/)
* [Semantic-UI](https://semantic-ui.com/introduction/getting-started.html)
* [Layout](https://blog.isquaredsoftware.com/2016/11/practical-redux-part-4-ui-layout-and-project-structure/#initial-ui-layout)
