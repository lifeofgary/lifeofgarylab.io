---
title: gatsby
date: 2020-01-15 12:31:07
tags: [gatsby, semantic-ui]
---

To install Gatsby with semantic-ui try

```
npx gatsby new website-name https://github.com/pretzelhands/gatsby-starter-semantic-ui
cd website-name
npx gatsby develop
```

For more details see https://medium.com/@rosegraner/how-to-integrate-semantic-ui-react-and-gatsby-js-36e8eaeb022b
