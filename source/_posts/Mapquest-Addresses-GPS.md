---
title: 'Mapquest: Addresses <=> GPS'
date: 2019-11-24 11:27:06
tags: ['Javascript', 'Node']
category: ['Javascript', 'Node']

---

Mapquest's api can be used to convert address to GPS coordinates and vica versa.  To do this a key
is needed from https://developer.mapquest.com/

The documentation can be found at https://developer.mapquest.com/documentation/open/geocoding-api/

Example of address to gps:
```
https://www.mapquestapi.com/geocoding/v1/address?key=${KEY}&inFormat=kvp&outFormat=json&location=5614+S+324th+Pl+Auburn+WA&thumbMaps=false
```

Example of gps to address:
```
https://open.mapquestapi.com/geocoding/v1/reverse?key=${KEY}&location=47.311037,-122.265073&includeRoadMetadata=true&includeNearestIntersection=true
```

5dd295cfd739f91aa869b51d
5dd295d0d739f91aa869b520
5dd296482842f11abfa31854

        {
            "$oid": "5ddb0dc66294ed6a9d791da0"
        },