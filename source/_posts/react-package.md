---
title: react-package
date: 2018-12-22 06:16:39
tags: ['Javascript', 'React']
category: ['Javascript']
---

The following steps are neeed to  create a node package from `create-react-app`.

1. Install a cross compiler
2. Create a compile task
3. Add code
4. Publish library for development
5. Test with an example project

# Install a cross compiler

Here are the commands

```bash
yarn add --dev babel-cli
yarn add --dev babel-preset-stage-3
yarn add --dev babel-preset-react-app
```
The quick explaination.

- `babel-cli` is a cross compiler which compiles jsx and es6 for the modern browser
- `babel-preset-stage-3` adds the spread operator (and other stuff)
- `babel-preset-react-app` adds the jsx compiling

Create the following file so babel uses the settings

In .babelrc
```json
{
  "presets": ["react", "stage-3"]
}
```

# Create a compile task

Add the following line to `package.json`.
```
    "compile": "NODE_ENV=production babel src --out-dir lib --copy-files"
```

The resulting section should be
```
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "compile": "NODE_ENV=production babel src --out-dir lib --copy-files"
  },
```
This compiles everything in `src/` and puts the output in `lib/`

# Add code

Put your code in `src/`.  Also create `/index.js` outside of src that points to `lib/`.  It should look like

index.js
```javascript
export {
  Test,
} from './lib'
```

# Publish library for development

The [yalc](https://github.com/whitecolor/yalc) helps with local npm development.  Add it globally

```bash
yarn global add yalc
```

The compile and deploy the library locally

```bash
yarn compile
yalc publish
```

This needs to be repeated each time you change the library.

# Test with an example project

Create a new project in another directory.  To import the library first add it to your `package.json`

```bash
yacl add <library_name>
```

Now in your code you can add the component with
```javascript
import {Test} from <library_name>
```

It uses `yalc`

In library

```bash
yalc publish
```

When using

```bash
yalc add muse-library
yarn
```

