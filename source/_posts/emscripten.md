---
title: Emscripten
date: 2018-05-29 14:34:42
tags:
---

# Emscripten 

Emscripten is a c++ to javascript cross compiler.  It can be installed following the instructions at https://kripken.github.io/emscripten-site/docs/getting_started/downloads.html

```
# Install Python
sudo apt-get install python2.7

# Install node.js
sudo apt-get install nodejs

# Get the emsdk repo
git clone https://github.com/juj/emsdk.git

# Enter that directory
cd emsdk

# Fetch the latest registry of available tools.
./emsdk update

# Download and install the latest SDK tools.
./emsdk install latest

# Make the "latest" SDK "active" for the current user. (writes ~/.emscripten file)
./emsdk activate latest

# Activate PATH and other environment variables in the current terminal
source ./emsdk_env.sh
```

It may be needed to change nodejs to node in `~/.emscripten`.  For example

```
NODE_JS='/home/gap/emsdk_portable/node/4.1.1_64bit/bin/node'
```
