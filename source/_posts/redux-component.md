---
title: redux-component
date: 2018-12-06 07:30:39
tags: ['Redux', 'Javascript']
category: ['Javascript']
---

To create a redux component it is needed to create a seperate store.  The file structure should look like:

```
ReduxContainer/
|-containers
  |-ReduxContainer.jsx
|-redux/
  |-reducer.js
|-index.js
|-RootReducer.js
|-Store.js
```

The files that will change are `containers/ReduxContainer.jsx` and `redux/reducer.js`.  References to them will also change.

index.js
```javascript
import React from 'react'
import { Provider } from 'react-redux'
import store from './Store.js'

import Container from './containers/ReduxContainer.jsx'

class ReduxContainer extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Container {...this.props }  />
      </Provider>
    )
  }
}

export default ReduxContainer

```

RootReducer.js
```javascript
import { combineReducers } from 'redux'
import reducer from './redux/reducer'

export default combineReducers({
    reducer
})
```

Store.js
```javascript
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from './RootReducer'
import thunk from 'redux-thunk'

const initialState = {}
const enhancers = []
const middleware = [
  thunk,
]

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
)

export default store
```

redux/reducer.js
```javascript
export const TEST = Symbol('TEST ACTTION')

export const test = payload => ({
  type: TEST,
  payload
})

const initialState = {
  test: 'test'
}

export default (state = initialState, action) => {
  let {type, payload} = action

  switch (type) {
    case TEST:
      return {...state, test: payload}
  }
  return state
}


containers/ReduxContainer.jsx
```javascript
import React from 'react'
import { connect } from 'react-redux'
import {
  test,
} from '../redux/reducer.js'

const ReduxContainer = props => <h1>Test</h1>

const mapStateToProps = (state) => ({
  reducer: state.reducer,
})

export default connect(mapStateToProps, {
  test
})(ReduxContainer)

```


