---
title: redux-quick-start-leaflest
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
date: 2019-01-11 07:10:31
---


* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)
* [Redux Leaflet](../redux-quick-start-leaflet)
* [Redux Immutable](../redux-quick-start-immutable)

# Add dependancies

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/17665e6bca4d8bc60f1ccf01e06a049a6636903a)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/17665e6bca4d8bc60f1ccf01e06a049a6636903a)
`git checkout 17665e6b`

```bash
add react-leaflet
add leaflet
cp -R node_modules/leaflet/dist/images public/
cp node_modules/leaflet/dist/leaflet.css public/
```

The reason for copying the images and css is to make markers show up.  They do not show up
when images are imported into the package.  They will show up if a CND in included in `public/index.html` or if they are copied into `src/public`.  The `lealet.css` can be imported into the js or required as shown below.

in `public/index.html` add
```
<link rel="stylesheet" href="./leaflet.css"/>
```

#h1 Add Leaflet menu item

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/f982226685d73ca6f469d271d5bc799627d3d6e6)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/f982226685d73ca6f469d271d5bc799627d3d6e6)
`git checkout f9822266`

Create src/component/Leaflet.jsx
```jsx
import React from 'react';

export default props=><h1>Leaflet</h1>
```

src/index.js
```jsx
import Leaflet from './component/Leaflet'
...snip...
            <Menu.Item as={NavLink} to='/leaflet'>
              Leaflet
            </Menu.Item>
...snip...
              <Route exact path='/leaflet' component={Leaflet}/>
...snip...
```

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/f982226685d73ca6f469d271d5bc799627d3d6e6)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/f982226685d73ca6f469d271d5bc799627d3d6e6)
`git checkout f9822266`

The important things here are:
1. `const position = [47.307323, -122.228455]` sets the geocoord of the map
2. `style={ {height: 300} }`
3. `<Marker position={position}>`
4. `<Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>`

Create src/component/Leaflet.jsx
```jsx
import React from 'react';

import { Map, Marker, Popup, TileLayer } from 'react-leaflet'

const position = [47.307323, -122.228455]
export default props => (
  <>
    <h1>Leaflet</h1>
    <Map center={position} zoom={13} style={ {height: 300} }>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
      />
      <Marker position={position}>
        <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
      </Marker>
    </Map>
  </>
)
```
