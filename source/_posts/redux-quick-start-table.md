---
title: redux-quick-start-table
date: 2019-01-05 07:34:04
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
---

This is part of the [React Quick Start](../redux-quick-start) series.  The articles are

* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)
* [Redux Leaflet](../redux-quick-start-leaflet)
* [Redux Immutable](../redux-quick-start-immutable)

```bash
yarn add redux-table
```

# React Table Css

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/afe9a75a75e17227c8c1e4b04bb5d7f5dd2c342b)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/afe9a75a75e17227c8c1e4b04bb5d7f5dd2c342b)
`git checkout afe9a75a`

In `src/index.js add
```js
import 'react-table/react-table.css'
```

# Simple Table

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/75994159f4ecfb880bd8ea3f58ef2c6a62b26ded)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/75994159f4ecfb880bd8ea3f58ef2c6a62b26ded)
`git checkout 75994159`

Now to create a table which has 3 columns.

1. <b>Name</b> A normal column
2. <b>Meta</b> A custom displayed column
3. <b>Nested</b> A column than displays nested data

Create `src/container/Table.jsx`
```jsx
import React from 'react'
import { connect } from 'react-redux'
import { actions } from '../redux/reducer.js'
import ReactTable from 'react-table'

class Table extends React.PureComponent {

  render() {
    const columns = [{
      Header: 'Name',
      accessor: 'name' // String-based value accessors!
    }, {
      Header: 'Meta',
      accessor: 'meta',
      Cell: props => <b>{props.value}</b> // Custom cell components!
    }, {
      id: 'nested', // Required because our accessor is not a string
      Header: 'Nested',
      accessor: item => item.nested.nest // Custom value accessors!
    }]

    const data = [{
      name: 'Item 1',
      meta: 'first item',
      nested: {
        nest: 'Nest for item1'
      }
    },{
      name: 'Item 2',
      meta: 'second item',
      nested: {
        nest: 'Nest for item2'
      }
    }]

    return <>
      <h1>Table</h1>
      <ReactTable/>
    </>
  }
}

export default Table
```

# Add a Table Menu

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/a9e1b33a4a65afa10846149517d580232ab6caa3)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/a9e1b33a4a65afa10846149517d580232ab6caa3)
`git checkout a9e1b33a`

Add to `src/index.js`
```jsx
import Table from './container/Table'
...snip...
          <Menu.Item as={NavLink} to='/table'>
            Table
          </Menu.Item>
...snip...
              <Route exact path='/table' component={Table}/>
```

# Table Properties

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/388d596487cf328f5be8b080b2bff7f391ad9c77)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/388d596487cf328f5be8b080b2bff7f391ad9c77)
`git checkout 388d5964`

Change a couple properties to clean out empty rows and add filters

Create `src/container/Table.jsx`
```jsx
      <ReactTable
        data={data}
        columns={columns}
        minRows={0}
        filterable = {true}
      />
```

Some of the properties are (for more search for `Props` [here](https://react-table.js.org/#/story/readme))
```js
{
  // General
  data: [],
  resolveData: data => resolvedData,
  loading: false,
  showPagination: true,
  showPaginationTop: false,
  showPaginationBottom: true
  showPageSizeOptions: true,
  pageSizeOptions: [5, 10, 20, 25, 50, 100],
  defaultPageSize: 20,
  minRows: undefined, // controls the minimum number of rows to display - default will be `pageSize`
  // NOTE: if you set minRows to 0 then you get rid of empty padding rows BUT your table formatting will also look strange when there are ZERO rows in the table
  showPageJump: true,
  collapseOnSortingChange: true,
  collapseOnPageChange: true,
  collapseOnDataChange: true,
  freezeWhenExpanded: false,
  sortable: true,
  multiSort: true,
  resizable: true,
  filterable: false,
  defaultSortDesc: false,
  defaultSorted: [],
  defaultFiltered: [],
  defaultResized: [],
  defaultExpanded: {},
  defaultFilterMethod: (filter, row, column) => {
    const id = filter.pivotId || filter.id
    return row[id] !== undefined ? String(row[id]).startsWith(filter.value) : true
  },
  defaultSortMethod: (a, b, desc) => {
    // force null and undefined to the bottom
    a = a === null || a === undefined ? '' : a
    b = b === null || b === undefined ? '' : b
    // force any string values to lowercase
    a = typeof a === 'string' ? a.toLowerCase() : a
    b = typeof b === 'string' ? b.toLowerCase() : b
    // Return either 1 or -1 to indicate a sort priority
    if (a > b) {
      return 1
    }
    if (a < b) {
      return -1
    }
    // returning 0, undefined or any falsey value will use subsequent sorts or
    // the index as a tiebreaker
    return 0
  },
  PadRowComponent: () => <span>&nbsp;</span>, // the content rendered inside of a padding row

}
```

# Load Async Data

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/77462fdadd449fd34dfc18f58624c375c5789b08)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/77462fdadd449fd34dfc18f58624c375c5789b08)
`git checkout 77462fda`

The follow needs to be done

* Add mock data (`public/table.json`)
* Create a reducer (`src/redux/table.js`)
* Add reducer to the root reducer (`src/RootReducer.js`)
* Update table (`src/container/Table.jsx`)

Create `public/table.json`
```json
[{
  "name": "Item 1",
  "meta": "first item",
  "nested": {
    "nest": "Nest for item1"
  }
},{
  "name": "Item 2",
  "meta": "second item",
  "nested": {
    "nest": "Nest for item2"
  }
},{
  "name": "Item 3",
  "meta": "third item",
  "nested": {
    "nest": "Nest for item3"
  }
}]
```

Create reducer `src/redux/table.js`
```js
export const LOAD_DATA          = Symbol("Load table data")

const loadData = text => async (dispatch, getState) => {
  try {
    let url = "table.json"
    let response = await fetch(url, {method: 'get'})
    let json = await response.json()
    dispatch({
      type: LOAD_DATA,
      payload: json
    })
  } catch (err) {
    console.log(err)
  }
}

export const actions = {
  loadData,
}

export const initialState = {
  data: [],
}

export default (state = initialState, action) => {
  let {type, payload} = action

  switch (type) {
    case LOAD_DATA:
      return {data: payload}
    default:
      return state
  }
}
```

Modify `src/RootReducer.js`
```js
import { combineReducers } from 'redux'
import reducer from './redux/reducer'
import table from './redux/table'

export default combineReducers({
  // add reducers here
  reducer,
  table,
})
```

Modify `src/container/Table.jsx`
```jsx
import React from 'react'
import { connect } from 'react-redux'
import { actions } from '../redux/table.js'
import ReactTable from 'react-table'

class Table extends React.PureComponent {
  constructor(props) {
    super(props)
    props.loadData()
  }

  render() {
    const {data} = this.props.table
    const columns = [{
      Header: 'Name',
      accessor: 'name' // String-based value accessors!
    }, {
      Header: 'Meta',
      accessor: 'meta',
      Cell: props => <b>{props.value}</b> // Custom cell components!
    }, {
      id: 'nested', // Required because our accessor is not a string
      Header: 'Nested',
      accessor: item => item.nested.nest // Custom value accessors!
    }]

    return <>
      <h1>Table</h1>
      <ReactTable
        data={data}
        columns={columns}
        minRows={0}
        filterable = {true}
      />
    </>
  }
}

const mapStateToProps = (state) => ({
  table: state.table,
})

export default connect(mapStateToProps, {
  ...actions
})(Table)
```

# References

* [React Table](https://react-table.js.org/#/story/readme)



