---
title: 'MongoDb'
tags: ['Javascript', 'Serverless', 'Node', 'MongoDb']
category: ['Javascript', 'Serverless', 'Node', 'MongoDb']
---

This is part of the [MongoDb](../mongo-configure) series.  The articles are

* [Mongo Configure](../mongo-configure)
* [Mongo Testing](../mongo-test)

install mongodb see https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

add [Mongoose](https://mongoosejs.com/) and add it to the package
```
yarn add mongoose
```

Configure mongoose.  Create `mongo_db.js`.  This will create a database named `my_database`
```javascript
let mongoose = require('mongoose');

// MONGO_URL is set in serverless.yml
let mongo_url  = process.env.MONGO_URL || 'mongodb://127.0.0.1/my_database'
mongoose.Promise = Promise;

let connect = () => mongoose.connect( mongo_url, { useNewUrlParser: true })
let disconnect = ()=>mongoose.disconnect()

if (process.env.NODE_ENV=="development")
  mongoose.set('debug', true)

module.exports.connect    = connect
module.exports.disconnect = disconnect
```

Now to create a model named `user.js`
```javascript
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let schema = new Schema({
//  _id: String,
  name: {
    // The type of the column  See https://mongoosejs.com/docs/schematypes.html#dates
    type: String,
    // it is required
    required: true,
    // it is trimed
    trim: true,
    // it has a unique index
    index: {
      unique: true,
      dropDups: true
      }
    },
})

let model
let modelName = "User"

if (mongoose.models[modelName])
  model = mongoose.model(modelName)
else
  model = mongoose.model(modelName, schema)

// Export model
module.exports = model
```

Now to populate the table user.  Create `seed.js`
```javascript

const {connect, disconnect} = require('./mongo_db');
const User = require('./user');
// connect to the database
connect();

const main = async () => {
  try {
    // Create user.
    let user = new User({
      name: "Gary",
    });
    await gary.save();
    console.log("Gary Created")
  } catch (err) {
    console.error(err)
  }
  // disconnect from the database
  disconnect()
}

main();
```

## Verify using mongo console
```
mongo my_database
> // Show all the databases
> show dbs
> // Show all the collections (tables) in the database
> show collections
> // Show all the records in users
> db.users.find();
```

## GeoCode

```bash
yarn add mongoose-geojson-schema
```

In the model add
```javascript
let GeoJSON = require('mongoose-geojson-schema');
...
let schema = new Schema({
//  _id: String,
  name: {type: String},
  location: { type: GeoJSON, required: true },
})
```

Then to seed
```javascript
    let user = new User({
      name: "Gary",
      location: {
        type: "Point",
        // https://www.latlong.net/convert-address-to-lat-long.html
        coordinates: [47.592260, -122.149200]
      }
    })
```

## References

https://www.tutorialspoint.com/mongodb/index.htm
https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications

### Geo

https://mongoosejs.com/docs/geojson.html
https://stackoverflow.com/search?q=%5Bmongoose%5D+geo
