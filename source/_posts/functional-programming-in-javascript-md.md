---
title: Functional Programming in Javascript
date: 2017-12-11 07:29:25
tags: ['Book', 'Javascript', 'Functional']
category: ['Books', 'Javascript']
stars: 3
difficulty: 5

---

<img src="https://images.manning.com/720/960/resize/book/5/c5bd123-f4fd-4a03-9069-9309c782ea7e/Atencio_hires_Fc.png" alt="Functional Programming in Javascript" style="width: 25%;"/>

_Functional Programming in Javascript_ teaches functional programming methods vs imperative programming.  Functional programming removes state from all functions so all functions can be thought as a deterministic function, each function does a transformation of the inputs and creates an output.  This makes unit testing easier.  Another key prinicple is to compose something complexe out of many similiar units.

# Notes

## Resources

* [Lodash](lodash.com) A utility library widely used in functional programming
* [Ramda](ramda.com) A utility libarary designed specifically for functional programming
* [RxJs])[https://github.com/Reactive-Extensions/RxJS] Javascript implementation of _reactive programming_ which combines the _observer pattern_, _iterator pattern_ and _functional programming_
* [Log4js](http://stritti.github.io/log4js) Powerful client-side logging
* [JSCheck](www.jscheck.org) Property based testing libarary based on Haskell's QuickCheck
* [Fantasy Land](https://github.com/fantasyland) Functional algebra library in JS

### Reactive programming books

* [Functional Reactive Programming](https://www.manning.com/books/functional-reactive-programming)
* [RxJS in action](https://www.manning.com/books/rxjs-in-action)

## Highlights

### Lens (pg 37)

Lens allow immutable manipulating attributes of stateful data types.

#### Example

```
var R = require('ramda')
var person = new Persion('Alonzo', 'Church', '333-33-3333')
var lastnameLens R.lenseProp('lastName')

// Read contents
R.view(lastnameLens, person) // -> 'Church'

// Create a modified object
var newPerson = R.set(latnameLense, 'Mourning, person)
newPerson.lastName // ->Mourning'
person.lastName    // ->Church'
```

## Higher order functions

Functions that are passed as parameters

### Chaining with Lodash (pg 73)

#### Example

```
_.chain(persons)
  .filter(isValid)
  .map( _.property('address.country)) // equivalent to R.view()
  .reduce(gatherStats, {})
  .values()
  .sortBy('count')
  .reverse()
  .first()
  .value()
  .name()
  ```

### Lodash Mixin (pg 75)

To create an sql like data structure that can be chained.  For more on [Javascript mixins](https://javascriptweblog.wordpress.com/2011/05/31/a-fresh-look-at-javascript-mixins)

#### Example

```
_.mixin({
  'select':  _.pluck,
  'from':    _.chain,
  'where':   _.fiter,
  'groupBy': _ .sortByOrder,
})

_.from(persons)
  .where(p=> p.birthYear>1900 )
  .groupBy( ['firstname', 'birthYear])
  .select('firstname', 'birthYear')
  .value()
```

### Piplines (pg 87)

A pipeline is a directional sequence of functions loosely arranged so that the output of one is the input to the next.

### Currying (pg 93)

Curring converts multivariable functions into a stepwise sequence of unary functions suspending execution until all arguments have been provided

```
curry(f) :: (a,b,c) -> f(a) -> f(b) -> f(c)
```

#### Example (pg 96)

```
const fetchStudentFromDb    = R.curry( (db, ssn)=>find(db,ssn) )
const fetchStudentFromArray = R.curry( (arr, ssn)=>arr(ssn))

const findStudent = useDb ? fetchStudentFromDb(db) : fetchStudentFromArray(arr)

findStudent('444-44-4444')
```

### Compose (pg 194)

_Functional composition_ is the process of grouping together complex behavior that have been broken into simpler tasks

#### Example
```
const trim = (str)=>str.replace(/^\s*|\s*/g, '') // Remove space before and after
const normalize = (str)=>str.replace(/\-/g, '')  // Remove dashes
const cleanInput = R.compose(normalize, trim)

cleanInput(' 444-44-4444')
```

### Useful combinators (functions) (pg 112)

* identity :: (a) -> a | Useful for supply data to higher order functions
* tap :: (a->*) -> a -> a | Useful for debugging or logging
* alternate returns one of two values
* sequence loops over an array and executes a function
* fork process data in multiple ways and recombine them

#### Examples

Fork

```
const fork = (join, func1, func2)=>{
  return func(val) {
    return join(func1(val), func2(val))
  }
}
}

const eqMedianAverage = fork(R.equals, R.median, R.mean)
eqMedianAverage( [80, 90, 100] ) //True
eqMedianAverage( [81, 90, 100] ) //False
```

### MONANDS

Monands functors that delegate special logic when handling certain cases

Promsies are monads

### Error handling (pg 132)

Error handling with Maybe and Either monads

#### Example

##### Maybe, Just, Nothing

```
class Maybe {
  static just(a) { return new Just(a); }
  static nothing { return new Nothing(); }
  static fromNullable(a) { return a!==null ? just(a) : nothing; }
  static of(a) { return just(a); }
  get isNothing() { return false; }
  get isJust() { return true; }
}

class Just extends Maybe {
  constructor(value) {
    super()
    this._value = value
  }
  get value() { return this._value; }
  map(f) [ return of(f(this.value)); ]
  getOrElse() { return this.value}
  filter(f) { Maybe.fromNullable( f(this.value) ? this.value : null )}
  get isJust() { return true}
  toString() { return `Maybe.Just(${this.value})`; }
}

class Nothing extends Maybe {
  map(f) { return this}
  get value() { throw new TypeError('Can not extract value of nothing`)}
  getOrElse(other) { return other; }
  filter() { return this.value; }
  get isNothing() { return true; }
  toString() { return 'Maybe.Nothing'}
}

// safeFindObject :: Db-> String -> Maybe
const safeFindObject = R.curry( (db, id)=>Maybe.fromNullable(find(db,id)) );
```

##### Either

Recovering from failure with either

```
class Either {
  constructor(value) {
    this._value = value
  }
  get value() { return this._value; }
  static left(a) { return new Left(a)}
  static right(a) { return new Right(a)}
  static fromNullable(val) { return val!==null ? right(val) : left(val); }
  static of(a) { return right(a); }
}

class Left extends Either {
  map(_) { return this; } // noop
  get value() { throw new TypeError('Can not extract value of Left().');}
  getOrElse(other) { return other; }
  orElse(f) { return f(this.value);}
  chain(f)  { return this}
  getOrElseThrow(a) { throw new Error(a); }
  filter(f) { return this; }
  toString() { return `Either.Left(${this.value})` }
 }

class Right extends Either {
  map(f) { return Eigher.of( f(this.value) ; } // return Right or Nothing on left
  getOrElse(other) { return this.value; }  // Extract right value or default
  orElse(f) { return this;}
  chain(f)  { return f(this.value); }
  getOrElseThrow(a) { this.value; }
  filter(f) { return Either.fromNullable( f(this.value) ? this.value : null; }
  toString() { return `Either.Right(${this.value})` }
 }

const safeFindObject = R.curry( (db, id) {
  const obj = find(db, id)
  if(obj)
    return Either.of(obj);
  return Either.Left('Object not found')
})

const findStudent = safeFindObject( DB9('student') )
findStudent('444-44-4444').getOrElse(new Student())
```

