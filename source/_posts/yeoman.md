---
title: yeoman
tags:
  - Javascript
  - Node
category:
  - Javascript
  - Node
difficulty: 2
date: 2018-12-06 07:13:03
---


[Yeoman](http://yeoman.io/) is a scaffolding tool for autogenerating boilerplate.  If you are unfamiliar with why to use yeoman see [Using yeoman change the way we work](https://css-tricks.com/using-yeoman-changed-way-work/)

## Getting Started

To create a yeoman project using the yoman generator to generate a generator use.  [Npx](https://www.npmjs.com/package/npx) downloads and runs a npm binary, `-p` defines which package to use.

```
npx -p yo -p generator-generator  -- yo generator
```

It will ask the following questions

- Your generator name
- Description
- Project homepage url
- Author's Name
- Author's Email
- Author's Homepage
- Package keywords
- Send coverage reports to coveralls
- Enter Node versions
- GitHub username or organization
- Which license do you want to use?

Most questions can be left blank.  For these instructions I will be using `example` as the generators name.  I use `No License (Copyrighted)` when creating generators for internal use.  It will create a directory `generator-example` for the project and install the yarn dependancies needed.

## First run

I have not figured out how to do a test run using npx so install yoman globally with `npm install yo -g` and then in the generators directory type `npm link` to link the generator to the global installs.  To do a test run type:

```bash
yo example
```

## Prompts

Take a look at `generators/app/index.js`.  It has 3 functions

- `prompts()` Ask for user input
- `writing()` Writes output
- `install()`

Yeoman uses [Inquirer](https://github.com/SBoudrias/Inquirer.js) for user prompts and here is the [list](https://github.com/SBoudrias/Inquirer.js#prompt-types).  Add an object the `prompts` to create another prompt.

Example

```javascript
   const prompts = [
      {
        type: "input",
        name: "schema_file",
        message: "What is the schema file?"
      },
      {
        type: "list",
        name: "schema_type",
        message: "What type is this?",
        choices: ["Input", "Output", "Router"]
      }
    ];
```

## Writing

This is where writing files is to happen.  Yeoman has an api for doing file system and template writes.  See[working with the filesystem](http://yeoman.io/authoring/file-system.html).  Here is a list of it's [methods](https://github.com/sboudrias/mem-fs-editor).

For now just write out the inputs

```javascript
  writing() {
    this.log(
      `Reading file ${this.props.schema_file} and generating a ${
        this.props.schema_type
      }`
    );
  }
```

## Install

This is for node dependancies via yarn.  See http://yeoman.io/authoring/dependencies.html  For projects that do not have change the code to

```javascript
  install() {
    /* Remove the code
    this.installDependencies();
    */
  }
```


## References

- http://yeoman.io/authoring/index.html
- https://scotch.io/tutorials/create-a-custom-yeoman-generator-in-4-easy-steps

