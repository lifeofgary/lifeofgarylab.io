---
title: redux-quick-start-router
date: 2019-01-05 04:41:30
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
---

This is part of the [React Quick Start](../redux-quick-start) series.  The articles are

* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)
* [Redux Leaflet](../redux-quick-start-leaflet)
* [Redux Immutable](../redux-quick-start-immutable)

# Install the package

```bash
yarn add react-router-dom
```

# Add a route

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/b711937c31259d08296bd68d41e348b3037b382f)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/b711937c31259d08296bd68d41e348b3037b382f)
`git checkout b711937c`

modify `src/index.js
```jsx
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import Container from './container/Container'


// Include semantic ui css from node
import 'semantic-ui-css/semantic.min.css';

render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={App}/>
        <Route exact path='/container' component={Container}/>
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
```

# Route Link
To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/585d9dd2a06697648d74a4d208e5166aa4f79bb1)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/585d9dd2a06697648d74a4d208e5166aa4f79bb1)
`git checkout 585d9dd2`

modify `src/App.js`
```jsx
import { Link } from 'react-router-dom'

class App extends React.Component {
  render() {
    return (
      <SemanticContainer>
        <Link to='/container'>Container</Link>
        <Container/>
      </SemanticContainer>
    );
  }
}
```

# Route Navigation

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/f3f6e6460798722c1d953993ad616d60da7aea00)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/f3f6e6460798722c1d953993ad616d60da7aea00)
`git checkout f3f6e646`

`SemanticContainer` need to be moved from `App` and put in the `Router`.  In addition a menu will be added at the top.

Modify `src/App.js`
```jsx
import React from 'react';
import Container from './container/Container'
import { Link } from 'react-router-dom'

class App extends React.Component {
  render() {
    return (
      <>
        <Link to='/container'>Container</Link>
        <Container/>
      </>
    );
  }
}
export default App;
```

Modify `src/index.js`
```jsx
import { Switch, Route, BrowserRouter, NavLink } from 'react-router-dom'
import Container from './container/Container'
import { Menu, Container as SemanticContainer} from 'semantic-ui-react';
...snip...
    <BrowserRouter>
      <>
        <Menu>
          <Menu.Item as={NavLink} to='/'>
            Home
          </Menu.Item>
          <Menu.Item as={NavLink} to='/container'>
            Container
          </Menu.Item>
        </Menu>
        <SemanticContainer>
          <Switch>
            <Route exact path='/' component={App}/>
            <Route exact path='/container' component={Container}/>
          </Switch>
        </SemanticContainer>
      </>
    </BrowserRouter>
```

# Route Structure

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/5d263aa4550302630683864c095bae9fd638be9e)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/5d263aa4550302630683864c095bae9fd638be9e)
`git checkout 5d263aa4`

Add html `<nav>` and `<body>` for better structure

Modify `src/index.js`
```jsx
      <>
        <nav>
          <Menu>
            <Menu.Item as={NavLink} to='/'>
              Home
            </Menu.Item>
            <Menu.Item as={NavLink} to='/container'>
              Container
            </Menu.Item>
          </Menu>
        </nav>
        <body>
          <SemanticContainer>
            <Switch>
              <Route exact path='/' component={App}/>
              <Route exact path='/container' component={Container}/>
            </Switch>
          </SemanticContainer>
        </body>
      </>
```
# Footer

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/54f592031784b8aff660233ba0117a302a4d3b09)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/54f592031784b8aff660233ba0117a302a4d3b09)
`git checkout 54f59203`

Modify `src/index.js`
```jsx
import { Menu, Container as SemanticContainer, Segment} from 'semantic-ui-react';
...snip...
        </body>
        <footer>
          <Segment attached='bottom'>copyright</Segment>
        </footer>
```

# References

* [React Router](https://reacttraining.com/react-router/)
* [Simple react router tutorial](https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf)
* [Semantic Layouts](https://react.semantic-ui.com/layouts)
* [Problems solve by Html5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_HTML_sections_and_outlines#Problems_solved_by_HTML5)

