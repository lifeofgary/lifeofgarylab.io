---
title: redux-quick-start
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
date: 2019-01-05 02:50:31
---

This is part of Gary's [React Quick Start](../redux-quick-start) series.  The articles are

* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)

1. [Create an app](#Create-an-app)
2. [Add Redux](#Add-redux)
3. [Create a component](#Create-a-component)
    * [Stateless component](#stateless-component)
    * [Pure component](#Pure-component)
4. [Mock data](#Mock-data)
5. [Create a reducer](#Create-a-reducer)
6. [Add an action](#Add-an-action)
    * [Simple action](#Simple-action)
    * [Asyncronous action](#Asyncronous-action)
7. [Create a container](#Create-a-container)
8. [Fire an action](#Fire-an-action)
    * [Fire without param](#Fire-without-paramenter)
    * [Fire with param](#Fire-with-paramenter)
    * [Fire an event](#Fire-an-event)

This assumes you know [React](https://reactjs.org/) and [Redux](https://redux.js.org/).  If you need to learn them I recommend [React Redux Tutorial for Beginners](https://www.valentinog.com/blog/redux/)
To follow this clone the quick-start lesson:

```bash
git clone git@gitlab.com:lifeofgary/react-quick-start.git
cd react-quick-start
git checkout eefd2ecf
yarn install
yarn start
```

or

```bash
git clone https://gitlab.com/lifeofgary/react-quick-start.git
cd react-quick-start
git checkout eefd2ecf
yarn install
yarn start
```
Each section will have a file link where you can review the files and a diff link where only the file changes are displayed.

# Create an app

```bash
npx create-react-app redux-quick-start
cd redux-quick-start
```

# Add Redux

Follow this on your own computer, but if you run into problems then you can compare your work against the repository
results with diff:
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/c20eafcae1d6d6d5c8abf7b71f740b7a5b4b4701)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/c20eafcae1d6d6d5c8abf7b71f740b7a5b4b4701)
`git checkout db77b1f0`

Add these packages with yarn:

```bash
yarn add redux
yarn add react-redux
yarn add redux-thunk
```
Add a reducer (a redux concept) to your React app:  Create `src/RootReducer.js`  All reducers will be added to it
```jsx
import { combineReducers } from 'redux'

export default combineReducers({
    // add reducers here
})
```

Create the Redux store.  `src/Store.js/
```jsx
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './RootReducer';
import thunk from 'redux-thunk';

const initialState = {};
const enhancers = [];
const middleware = [thunk];

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
```

edit `src/index.js` to
```js
import React from 'react'
import App from './App'
import * as serviceWorker from './serviceWorker';

import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './Store.js'

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
```


# Create a component

Follow this on your own computer, but if you run into problems then you can compare your work against the repository
results with diff:
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/bd84138781f12e1ae12bcb53433b448e5c3d615b)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/bd84138781f12e1ae12bcb53433b448e5c3d615b)
`git checkout bd841387`


We won't be using these files, so delete them:
```bash
rm src/logo.svg
rm src/App.css
rm src/serviceWorker.js
```

modify `src/App.js` from the default to our project which will contain two components, named Pure and Stateless)
```jsx
import React from 'react';
import Stateless from './component/Stateless'
import Pure from './component/Pure'

class App extends React.Component {
  render() {
    return (
      <div>
        <Stateless header="Stateless" text="Stateless text"/>
        <Pure header="Pure" text="Pure text"/>
      </div>
    );
  }
}

export default App;
```

## Stateless component

If you're not using life cycle methods with a component, then make it a stateless one.

Create `src/component/Stateless.jsx`

```jsx
import React from 'react';

export default props => {
    const {header, text} = props

    return (
        <>
            <h1>{header}</h1>
            <p>{text}</p>
        </>
    )
}
```

## Pure component

These are used for the [Lifecycle](https://reactjs.org/docs/react-component.html#the-component-lifecycle)
Stateless components can't use the life cycle methods, but Pure components can.

Create `src/component/Pure.jsx`
```jsx
import React from 'react';

class Pure extends React.PureComponent {
    // Helper functions need to be written as follows so this is correct
    // helper = () => <h2>Child</h2>
    render() {
        const {header, text} = this.props

        return (
            <>
                <h1>{header}</h1>
                <p>{text}</p>
            </>
        )
    }
}

export default Pure
```

# Mock data

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/a9928a369f84461adc988d20b2db7b5aedbd5c2a)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/a9928a369f84461adc988d20b2db7b5aedbd5c2a)
`git checkout a9928a36`

Create `public/pure.txt`
```
Pure text is changed.
```

# Create a reducer

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/a9928a369f84461adc988d20b2db7b5aedbd5c2a)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/a9928a369f84461adc988d20b2db7b5aedbd5c2a)
`git checkout 01d70cd7`

and

[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/5ed8f61d89e8b94de90c765efc85f1e82da1ec00)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/5ed8f61d89e8b94de90c765efc85f1e82da1ec00)
`git checkout 5ed8f61d`

Create `src/redux/reducer.js`  Since there's only one reducer in this example, we simply call it reducer.
```jsx
export const actions = {
}

export const initialState = {
  pure_text: "Pure text",
  stateless_text: "State text",
}

export default (state = initialState, action) => {
  return state
}
```

Add the reducer to the `src/redux/RootReducer.js` so it looks like
```js
import { combineReducers } from 'redux'
import reducer from './redux/reducer'

export default combineReducers({
  // add reducers here
  reducer
})
```

# Create a container

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/3ce309cb82ca56ac95ccc0310dc0733a2d2a69ed)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/3ce309cb82ca56ac95ccc0310dc0733a2d2a69ed)
`git checkout 3ce309cb`

The next thing we need for Redux is a container.  connect and actions are
mandatory imports; Stateless and Pure are the components we want it to include.

Add `src/container/Container.jsx`
```jsx
import React from 'react'
import { connect } from 'react-redux'
import { actions } from '../redux/reducer.js'
import Stateless from '../component/Stateless'
import Pure from '../component/Pure'

let Container = props => {
    let {pure_text, stateless_text} = props.reducer
    return <div>
        <h1>Container</h1>
        <Stateless header="Stateless" text={stateless_text}/>
        <Pure header="Pure" text={pure_text}/>
    </div>
}

const mapStateToProps = (state) => ({
    reducer: state.reducer,
  })

export default connect(mapStateToProps, {
    ...actions
})(Container)
```

Modify `src/App.js` to Put the new container in the app.
```jsx
import React from 'react';
import Container from './container/Container'

class App extends React.Component {
  render() {
    return (
      <Container/>
    );
  }
}

export default App;
```

# Add an action
## Simple action
To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/feb1855d6f58bc24b21a6fcf8016601ee350e04e)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/feb1855d6f58bc24b21a6fcf8016601ee350e04e)
`git checkout feb1855d`

Edit `src/redux/reducer/js`
```jsx
export const SET_STATELESS_TEXT  = Symbol("Stateless text")

const setStatelessText = text => ({
  type: SET_STATELESS_TEXT,
  payload: text,
})


export const actions = {
  setStatelessText,
}

export const initialState = {
  pure_text: "Pure text",
  stateless_text: "State text",
}

export default (state = initialState, action) => {
  let {type, payload} = action

  switch (type) {
    case SET_STATELESS_TEXT:
      return {...state, stateless_text: payload}
    default:
      return state
  }
}
```

## Asyncronous action
To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/9a4b092a53a8db96d87655abc7edea06c59d0e46)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/9a4b092a53a8db96d87655abc7edea06c59d0e46)
`git checkout 9a4b092a`

Add the following to `src/redux/reducer/js`
```jsx
export const SET_STATELESS_TEXT  = Symbol("Stateless text")
...snip...
const setPureText = text => async (dispatch, getState) => {
  try {
    let url = "pure.txt"
    let response = await fetch(url, {method: 'get'})
    text = await response.text()
    dispatch({
      type: SET_PURE_TEXT,
      payload: text
    })
  } catch (err) {
    console.log(err)
  }
}

export const actions = {
  setStatelessText,
  setPureText,
}
...snip...
export default (state = initialState, action) => {
...snip...
    case SET_STATELESS_TEXT:
      return {...state, stateless_text: payload}
...snip...
}
```

# Fire an action

##Fire without paramenter
To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/41ef54bcbd38d1099bfc29f5050050eaa1b3193b)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/41ef54bcbd38d1099bfc29f5050050eaa1b3193b)
`git checkout 41ef54bc`

In `src/redux/reducer.js` updated `setStatelessText` to not require a parmater

```jsx
const setStatelessText = () => ({
  type: SET_STATELESS_TEXT,
  payload: "State text is updated",
})
```

In `src/container/Container` add a button

```
...snip...
        <Stateless header="Stateless" text={stateless_text}/>
        <button onClick={props.setStatelessText}>
            Update Stateless
        </button>
...snip...
```

## Fire with paramenter
To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/6394909e9eeae91c0d9b1ebc76247f20ad0d5126)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/6394909e9eeae91c0d9b1ebc76247f20ad0d5126)
`git checkout 6394909e`

In `src/container/Container` add a button.  This requires an anonymous function to call the action creator.

```
...snip...
        <Pure header="Pure" text={pure_text}/>
        <button onClick={()=>props.setPureText("Update Pure Text.")}>
            Update Pure
        </button>
...snip...
```

## Fire an event
To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/4f3afe4a5551ccdfa32d339510d24da638e3b931)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/4f3afe4a5551ccdfa32d339510d24da638e3b931)
`git checkout 4f3afe4a`

Fired events are [Sythentic events](https://reactjs.org/docs/events.html#supported-events)

In `src/redux/reducer.js` do the following

#### 1. Add an action

```jsx
export const MOUSE_MOVE          = Symbol("Mouse Move")
```

#### 2. Add an action creator

```jsx
const mouseMove = (x,y) => ({
  type: MOUSE_MOVE,
  payload: {x,y},
})
```

#### 3. Update initial state

```jsx
export const initialState = {
  pure_text: "Pure text",
  stateless_text: "State text",
  x: "-",
  y: "-",
}
```

#### 4. Modify the reducer

```jsx
    case MOUSE_MOVE:
      let {x,y} = payload
      return {...state, x, y}
```

In `src/container/Container` add

#### 1. A canvas that can be hovered over and some text to display the mouse coordinates

```jsx
<hr/>
{x}, {y} <br/>
<canvas
    width="100"
    height="100"
    style={ {backgroundColor: "bisque"} }
    onMouseMove={e=>props.mouseMove(e.clientX, e.clientY)}
/>
```
#### 2. Extract x, y from props

```jsx
let {pure_text, stateless_text, x ,y} = props.reducer
```

<b>Hint</b>: I usually print the event and inspect it to find what the passed options are via console inspection.  This is
done with the following code.

```jsx
    onMouseMove={e=>console.log(e)}
```

# References
* [React](https://reactjs.org/)
* [Redux](https://redux.js.org/)
* [Redux Store](https://redux-docs.netlify.com/recipes/configuring-your-store)
* [React Router](../redux-quick-start-router)

Future Topics and Libraries

* [Immutable](https://www.npmjs.com/package/immutable)
* [Ramda](https://ramdajs.com/) [Tutorial](http://randycoulman.com/blog/2016/05/24/thinking-in-ramda-getting-started/)
* [react-leaflet](https://www.npmjs.com/package/react-leaflet)




