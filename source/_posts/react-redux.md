---
title: react-redux
date: 2018-02-26 07:21:27
tags: ['Redux', 'Javascript', 'React']
category: ['Javascript']

---

```
npx create-react-app castus
cd castus
yarn add redux react-redux react-router-dom react-router-redux@next redux-thunk history
```

Following the instructions at https://medium.com/@notrab/getting-started-with-create-react-app-redux-react-router-redux-thunk-d6a19259f71f

## store.js

```
import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './rootReducer'

export const history = createHistory()

const initialState = {}
const enhancers = []
const middleware = [
  thunk,
  routerMiddleware(history)
]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
)

export default store

```

## rootReducer.js
```
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import counter from './redux/counter'

export default combineReducers({
  routing: routerReducer,
  counter,
})

```

## containers/App.jsx
```
import React from 'react';
import { Route, Link } from 'react-router-dom'
import Home from './home'
import About from '../components/about'

const App = () => (
  <div>
    <header>
      <Link to="/">Home</Link>
      <Link to="/about-us">About</Link>
    </header>

    <main>
      <Route exact path="/" component={Home} />
      <Route exact path="/about-us" component={About} />
    </main>
  </div>
)

export default App
```

# containers/Home.jsx
```
import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  increment,
} from '../redux/counter'

const Home = props => (
  <div>
    <h1>Home</h1>
    <p>Welcome home!</p>

    <p>Count: {props.count}</p>
    <p>
      <button onClick={props.increment} disabled={props.isIncrementing}>Increment</button>
    </p>

    <button onClick={() => props.changePage()}>Go to about page via redux</button>

  </div>
)

const mapStateToProps = state => ({
  count: state.counter.count,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  increment,
  changePage: () => push('/about-us')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)

```

## components/About.jsx

```

import React from 'react'

export default () => (
  <div>
    <h1>About Us</h1>
    <p>Hello Medium!</p>
  </div>
)
```

## index.js
```
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'
import App from './containers/app'

const target = document.querySelector('#root')

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <App />
      </div>
    </ConnectedRouter>
  </Provider>,
  target
)
```

## redux/counter.js
```
export const INCREMENT = Symbol('INCREMENT')

const initialState = {
  count: 0,
}

export default (state = initialState, action) => {
    let {type, payload} = action

    switch (type) {
    case INCREMENT:
      return {
        ...state,
        count: state.count + payload,
      }

    default:
      return state
  }
}

export const increment = () => {
  return (dispatch, state) => {
    dispatch({
      type: INCREMENT,
      payload: 1,
    })
  }
}

```

