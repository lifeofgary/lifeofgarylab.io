---
title: redux-quick-start-forms
date: 2019-01-08 06:15:25
tags:
  - Redux
  - Javascript
  - React
category:
  - Javascript
  - React-Quick-Start
---

This is part of the [React Quick Start](../redux-quick-start) series.  The articles are

* [Getting Started](../redux-quick-start)
* [Semantic-UI](../redux-quick-start-semantic)
* [React Router](../redux-quick-start-router)
* [React Table](../redux-quick-start-table)
* [Redux Forms](../redux-quick-start-forms)
* [Redux Leaflet](../redux-quick-start-leaflet)
* [Redux Immutable](../redux-quick-start-immutable)

Next is to add [redux-forms](https://redux-form.com).  It automatically manages a form so there is no need to create a redux reducde.  This saves a lot of work.  To get started we will follow this [guide](https://redux-form.com/7.4.2/docs/gettingstarted.md/#basic-usage-guide)

Install the library
```bash
yarn add redux-form
```

# Create a Table Container

To follow
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/5cccfe269dfb78016e304b440632e0b4f9008afb)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/5cccfe269dfb78016e304b440632e0b4f9008afb)
`git checkout 5cccfe26`

Create a placeholder file `src/container/Form.jsx`
```jsx
import React from 'react'
import {Button} from 'semantic-ui-react'

let Form = props => {

    return <>
        <h1>Form</h1>
        User: <br/>
        Password: <br/>

        <form className="ui form">
            <input type="text" />
            <input type="password" />
            <Button>Submit</Button>
        </form>
    </>
}

export default Form
```

Connect the Form to the menu

Add to `src/index.js`
```jsx
import Form from './container/Form'
...snip...
            <Menu.Item as={NavLink} to='/form'>
              Form
            </Menu.Item>
...snip...
              <Route exact path='/form' component={Form}/>
```



# Add reducer to RootReducer
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/d4c96258436aaf6fb43a1d007379f4ed2eb57d9d)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/d4c96258436aaf6fb43a1d007379f4ed2eb57d9d)
`git checkout d4c96258`

Add `import { reducer as formReducer } from 'redux-form'` and `  form: formReducer` to `src/RootReducer.js`
```jsx
import { combineReducers } from 'redux'
import reducer from './redux/reducer'
import table from './redux/table'
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
  // add reducers here
  reducer,
  table,
  form: formReducer
})
```

# Connect Form Reducer
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/76c9874e771626ee706d1c182ed892849dcd1be8)
[files](https://gitlab.com/lifeofgary/react-quick-start/tree/76c9874e771626ee706d1c182ed892849dcd1be8)
`git checkout 76c9874e`

Thisis done by wrapping the form elements in `Field` and wrapping the form in `reduxForm`.  The `{form: user}` names the reducer in the state as user.  It will be accessed in state as `form.user`.  The form is from the root reducer.

Update `src/container/Form.jsx`
```jsx
import React from 'react'
import {Button} from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'

let Form = props => {
    return <>
        <h1>Form</h1>
        User: <br/>
        Password: <br/>

        <hr/>

        <form className="ui form">
            <div>
                <label>Username Name</label>
                <Field name="user_name" component="input" type="text" />
            </div>
            <div>
                <label>Password</label>
                <Field name="password" component="input" type="password" />
            </div>
            <Button>Submit</Button>
        </form>
    </>
}

Form = reduxForm({
    // a unique name for the form
    form: 'login'
  })(Form)

export default Form
```

# Connect to State
[diff](https://gitlab.com/lifeofgary/react-quick-start/commit/9b24b7f5335c4ad187e03d86c8799f1792de6655)
[files](https://gitlab.com/lifeofgary/react-quick-start/commit/9b24b7f5335c4ad187e03d86c8799f1792de6655)
`git checkout 9b24b7f5`

The form can be connected to the form reducer.  This is going to be double
nested in props which can be a little confusing.

Update `src/container/Form.jsx`
```jsx
import React from 'react'
import { connect } from 'react-redux'
import {Button} from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'

let Form = props => {
    console.log(props.login_form.login)
    let user_name =  <i>'none'</i>
    let password =  <i>'none'</i>

    if (props.login_form.login) {
        let { values } = props.login_form.login

        if (values) {
            user_name = values.user_name || user_name
            password = values.password || password
        }
    }
    return <>
        <h1>Form</h1>
        User: {user_name} <br/>
        Password: {password} <br/>

        <hr/>

        <form className="ui form">
            <div>
                <label>Username Name</label>
                <Field name="user_name" component="input" type="text" />
            </div>
            <div>
                <label>Password</label>
                <Field name="password" component="input" type="password" />
            </div>
            <Button>Submit</Button>
        </form>
    </>
}

Form = reduxForm({
    // a unique name for the form
    form: 'login'
  })(Form)

const mapStateToProps = (state) => ({
    login_form: state.form,
})

Form = connect(mapStateToProps, {
})(Form)

export default Form
```



