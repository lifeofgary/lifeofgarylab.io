---
title: react-setup
date: 2019-01-08 08:35:37
tags: ['Redux', 'Javascript', 'React']
category: ['Javascript']
---

# Setting up a computer

This assumes you've got a Windows computer with Windows 10 newly installed and registered with Microsoft.  These are the steps to get a Javascript React app built and working.

- Install and enable Ubuntu (unix) for Windows.  This requires both getting the program from the Microsoft store, and enabling a Windows system option.  Follow instructions starting at "Install from the Microsoft Store" header at [install the Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

**Do the following steps in a ubuntu bash shell:**

- Install nvm, the Node version manager: `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh`
- Use nvm to install Node.  Node is a tool that can run Javascript from a command line.  It's referred to variously as node and nodejs, which can lead to name problems.  `nvm  install 8`  If version 8 doesn't work, consider using version 10, the latest as of this writing.
- Use npm to install yarn. (npm is part of nvm)  yarn is a package manager just for react projects.  As of this writing it wasn't working, so we're using npm instead.  But we still need npx, which is included with yarn.  Here's how to install it: `npm install yarn -g`


**The following steps come from the tutorial in the online blog site Medium:** [Getting started with create-react-app...](https://medium.com/@notrab/getting-started-with-create-react-app-redux-react-router-redux-thunk-d6a19259f71f)
- Use npx and create-react-app to create the new app in boilerplate form.
*  Navigate to parent directory of where you'll create the new project `cd /mypath/my_parent_dir`
* npx downloads a node binary from npmjs.com and runs it from a temp directory, something like a Docker instance so you don't have to install it globally for one-time use. `npx create-react-app react-redux-example`
* go into the new directory `cd react-redux-example`
* Use npm to install all the packages the new project requires.  The package list is found in file packages.json `npm install` or `yarn install` or
* To set up packages that aren't in the list yet, `yarn add new-package-names`
* To run the new app, you should still be in the app's root directory. `yarn start` or `npm start`.  This starts a local server instance on port 3000, and opens the default browser (Microsoft Edge in a new computer) to this location.
* If your package setup gets corrupted, delete the node_modules directory and redo the yarn install:  `rm -Rf node_modules/`.  If the yarn server instance isn't working, you can fall back on the more generic html-server:  `npx html-server`  This serves on port 8080 instead of 3000.


**More Windows installations**

- Install Notepad++ for general text editing [Notepad++ home page](https://notepad-plus-plus.org/download/) You probably want to select the 64-bit Installer for whatever is the current version.
- Install Visual Studio Code as an IDE [download site](https://code.visualstudio.com/Download)
- Install a different Web browser such as Firefox if you like.  In all major browsers, **F12** opens a Javascript debugger and the debuggers are all very similar.

