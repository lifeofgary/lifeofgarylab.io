---
title: 'MongoDb'
tags: ['Javascript', 'Serverless', 'Node', 'MongoDb']
category: ['Javascript', 'Serverless', 'Node', 'MongoDb']
---

This is part of the [MongoDb](../mongo-configure) series.  The articles are

* [Mongo Configure](../mongo-configure)
* [Mongo Testing](../mongo-test)


Install an in memory version of mongodb

```
yarn add mongodb-memory-server
```

Modify the configuration to add a helper for starting and stoping the mock server.
Also, if the in memory server has started, connect to it.
```javascript
let mongoose = require('mongoose');

// MONGO_URL is set in serverless.yml
mongoose.Promise = Promise;

let mongod, test_uri

module.exports.startMockServer = async () => {
  const { MongoMemoryServer } = require('mongodb-memory-server');
  mongod = new MongoMemoryServer()
  try {
    test_uri = await mongod.getConnectionString();
  } catch (err) {
    process.exit(1);
  }
}

module.exports.stopMockServer = () => {
  console.log("Stopping server")
  mongod.stop();
}

module.exports.connect = ({debug=false}={}) =>{
  if (process.env.NODE_ENV=="development" || debug)
    mongoose.set('debug', true)

  let mongo_url  = test_uri || process.env.MONGO_URL || 'mongodb://127.0.0.1/my_database'
  console.log(`connecting to ${mongo_url}`)

  mongoose.connect( mongo_url, { useNewUrlParser: true })
}

module.exports.disconnect = ()=>mongoose.disconnect()
```

Break seed.js into two parts so the tests can use the seed data with the in version copy of ubuntu

seed.js
```javascript

const {create_users} = require('./seed_data')

const main = async () => {
  // From https://mongoosejs.com/docs/
  connect();


  try {
    await create_users({print: true});
  } catch (err) {
    console.error(err)
  }

  disconnect();
}

main();
```

seed_data.js
```javascript
const {connect, disconnect} = require('../mongo/mongo_db');

const User = require('../mongo/models/user');
const {hashPassword, validatePassword} = require('../mongo/helpers');

module.exports.create_users = async ({print = false}={}) => {
  let gary = new User({
    name: "Gary",
    password: hashPassword("Password"),
    email: "gap@eskimo.com",
  });
  await gary.save();
  if (print) console.log("Agent Gary Created")
};
```

Create test

```javascript

const {expect} = require('chai')
const {MongoMemoryServer} = require('mongodb-memory-server');
const {startMockServer, stopMockServer, connect, disconnect} = require('../mongo/mongo_db')
const {all} = require('../src/agent')

const {create_users} = require('../scripts/seed_data')

describe('Array', function() {
  it('should return -1 when the value is not present', function() {
    let array = [1,2,3];

    expect( array.indexOf(4) ).to.equal(-1);
  });
});

describe('#indexOf()', function() {
  let mongod

  before(async () => {
    await startMockServer();
    connect();
    await create_users();
  })

  after('stuff',()=>{
    disconnect();
    stopMockServer();
  })

  it ('should have one user', async()=>{
    let users  = await User.find({}).lean()
    expect(users.length).to.equal(1);
  })

});
```
