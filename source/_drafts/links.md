---
title: links
tags:
---

- [react component preformance](https://codeburst.io/when-to-use-component-or-purecomponent-a60cfad01a81)
- [Category Theory](https://bartoszmilewski.com/2014/11/04/category-the-essence-of-composition/)

-[Serverless step functions](https://medium.freecodecamp.org/building-a-community-sign-up-app-with-serverless-stepfunctions-and-stackstorm-exchange-episode-6efb9c102b0a)


- [Serverless: Stage variables](https://serverless-stack.com/chapters/stages-in-serverless-framework.html)
- [Serverless: Setting up a domain](https://serverless.com/blog/serverless-api-gateway-domain/)
- [Serverless: More than one domain](https://stackoverflow.com/questions/46956660/how-to-set-up-different-domains-based-on-stage-with-serverless-domain-manager-pl)
- [AWS Https Certificate](https://www.pandastrike.com/posts/20160613-ssl-cert-aws-api-gateway-zerossl-letsencrypt)

- [React HOC](https://medium.com/@franleplant/react-higher-order-components-in-depth-cf9032ee6c3e)


- https://ponyfoo.com/articles/tagged/es6-in-depth
- [Rect Native Libraries](https://blog.bitsrc.io/11-react-native-component-libraries-you-should-know-in-2018-71d2a8e33312)
- [React](https://daveceddia.com/)
- [Ramda and Reselect](https://medium.com/hexient-labs/redux-ramda-reselect-recompose-41171dcf63fd)
- [React Resources](https://codeutopia.net/blog/2017/04/03/230-curated-resources-and-tools-for-building-apps-with-react-js/)
- [Core Web Technologies (Html, Css and Javascript)](https://developer.mozilla.org/en-US/docs/Web/Tutorials)
- [React: Ui framework](https://reactjs.org/tutorial/tutorial.html)
- [Semantic UI React](https://react.semantic-ui.com/)
- [Redux Videos](https://egghead.io/courses/building-react-applications-with-idiomatic-redux)
- https://www.leighhalliday.com/testing-react-jest-enzyme-sinon
- https://www.hillelwayne.com/post/formally-specifying-uis/
- [Digital Fashion Pro](https://startmyline.com/dfp/)
- [Using Javascript in Wordpress](https://codex.wordpress.org/Using_Javascript)
- [Lori's walk with me](https://www.youtube.com/channel/UC4CGqA4RzI_Wk9yq-fe0-AQ)
- [Semantic Ui Colors](https://codepen.io/audreyr/pen/yYVPNQ)
- [Functional Programming Book](https://drboolean.gitbooks.io/mostly-adequate-guide/content/)
- [Interesting JS and CSS Libraries List](https://tutorialzine.com/articles)
- [JS Doc](https://resin.io/blog/open-source-guide-1-documentation-and-jsdoc/)

- [Duktape fix for C++ Builder](http://stackoverflow.com/questions/33283874/inttypes-h-file-not-found-compiliing-duktape-with-c-builder-and-clang)

# Pattern Drafting instructions
- [Wide variety of clothing](https://wkdesigner.wordpress.com/tag/patterndrafting/)
- [Tie](https://wkdesigner.wordpress.com/2009/05/28/father-son-ties/)

# Web Cameras
- https://www.amazon.com/Microsoft-LifeCam-Cinema-Webcam-Business/dp/B004ABQAFO/ref=sr_1_3?ie=UTF8&qid=1533851519&sr=8-3&keywords=hd+webcam+microsoft
- https://www.amazon.com/Logitech-960-000585-HD-Webcam-C310/dp/B003LVZO8S/ref=sr_1_24?ie=UTF8&qid=1533851767&sr=8-24&keywords=hd+webcam+microsoft

# Transcribe Software
- https://www.seventhstring.com/xscribe/buy.html
- https://otranscribe.com/
- http://intonia.com

# Politics
- https://hiddentribes.us/pdf/hidden_tribes_report.pdf

# Food

- https://www.theatlantic.com/science/archive/2019/01/what-its-like-be-allergic-corn/580594/
- http://corn-freefoods.blogspot.com/2017/11/corn-allergen-list-corn-derivatives.html
- https://cornallergygirl.com/2019/01/10/clearing-up-some-misconceptions-about-corn-allergy/#more-3723
- [tabouli](http://ix.io/66H)
- [Tom Kha Gai Soup](https://www.myrecipes.com/recipe/thai-chicken-coconut-soup-tom-kha-gai)
- [Instant Pot Recipes](http://www.countryliving.com/food-drinks/g5040/best-instant-pot-recipes/)

# React Leaflet

- https://www.azavea.com/blog/2016/12/05/getting-started-with-react-and-leaflet/
- https://jsfiddle.net/paul_lecam/q2v7t59h/
- https://stackoverflow.com/questions/40719689/how-to-include-leaflet-css-in-a-react-app-with-webpack

# Interview Questions


https://www.hiredintech.com/classrooms/system-design/lesson/60

# Virtual Servers

- https://lowendbox.com/blog/techvps-1gb-kvm-vps-for-18-year-more-w-free-windows-os-in-los-angeles-ny/

# Visual Studio Code - JS Breakpoints

https://blogs.msdn.microsoft.com/cdndevs/2015/12/21/visual-studio-code-debugger-for-chrome-extension/

needs a breakpoint statement

```
{
    "name": "Launch localhost with sourcemaps",
    "type": "chrome",
    "request": "launch",
    "url": "http://localhost:3000",
    "sourceMaps": true,
    "webRoot": "wwwroot",
    "runtimeExecutable": "/usr/bin/chromium-browser",
    "runtimeArgs": ["--remote-debugging-port=9222"]
}
```


# Valgrind
```
 valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes ./test -v --track-origins=yes
 ```
# Batch Compiling in C++ Builder
```
Select Project > Options > C++ Compiler, and enable the "Enable batch compilation" option under General Compilation.
Select Project > Options > Project Properties and:
    Enable the "Run C++ compiler in a separate process" option.
    Select the number of parallel subprocesses that you want to use.

Note: The number of subprocesses that you choose cannot be higher than the number of cores of your CPU.
```

# Electron Print
```
    // Create the browser window.
    mainWindow = new BrowserWindow({width: 800, height: 600});
    log.error("Testing print")
    log.error( "===")
    log.error( mainWindow.webContents.getPrinters() )
    log.error( "=====")
```

# Haskell Music
```
cabal install Euterpea

apt-get install libasound2-dev

 1997  sudo apt-get install haskell-platform
 1998  fluidsynth
 1999  sudo apt install fluidsynth
 2000  cabal install Euterpea
 2001  asound
 2002  apt install ecasound
 2003  sudo apt install ecasound
 2004  cabal install Euterpea
 2005  sudo apt install libportmidi0
 2006  cabal install Euterpea
 2007  qsynth
 2008  jack
 2009  sudo apt install ajck
 2010  sudo apt install jack
 2011  jack
 2012  cabal install Euterpea
 2013  sudo apt-get install libasound2-dev
 2014  cabal install Euterpea
 ```

# Semantic UI Colors
```
/*******************************
         Site Settings
*******************************/

/*-------------------
        Paths
--------------------*/

@imagePath : "../../themes/default/assets/images";
@fontPath  : "../../themes/default/assets/fonts";

/*-------------------
       Fonts
--------------------*/

@headerFont    : "Open Sans", "Helvetica Neue", Arial, Helvetica, sans-serif;
@pageFont      : "Open Sans", "Helvetica Neue", Arial, Helvetica, sans-serif;
@fontSmoothing : antialiased;

/*-------------------
      Site Colors
--------------------*/

/*---  Colors  ---*/

@blue             : #0074D9;
/*
Light 'PETER RIVER' '#3498DB'
Dark  'BELIZE HOLE'  '#2980B9'
*/

@green            : #2ECC40;
/*
Light 'EMERALD'   '#2ECC71'
Dark  'NEPHRITIS' '#27AE60'
*/

@orange           : #FF851B;
/*
'CARROT'  '#E67E22'
'PUMPKIN' '#D35400'
*/

@pink             : #D9499A;
/*
Same as red
'ALIZARIN'  '#E74C3C'
'POMEGRANATE'  '#C0392B'
*/

@purple           : #A24096;
/*
Light 'AMETHYST'  '#9B59B6'
Dark  'WISTERIA'  '#8E44AD'
*/

@red              : #FF4136;
/*
Same as pink
'ALIZARIN'  '#E74C3C'
'POMEGRANATE'  '#C0392B'
*/

@teal             : #39CCCC;
/*
Light 'TURQUOISE'  '#1ABC9C'
Dark 'GREEN SEA' '#16A085'
*/

@yellow           : #FFCB08;
/*
Light 'SUN FLOWER'  '#F1C40F'
Dark 'ORANGE'  '#F39C12'
*/

@black            : #191919;
/*
Light 'WET ASPHALT'   '#34495E'
Dark  'MIDNIGHT' '#2C3E50'
*/

@grey             : #CCCCCC;
/*
Light 'CONCRETE' '#95A5A6'
Dark  'ASBESTOS' '#7F8C8D'
*/

@white            : #FFFFFF;
/*
Light 'CLOUDS' '#ECF0F1'
Dark  'SILVER' '#BDC3C7'
*/

/*---  Light Colors  ---*/
@lightBlue        : #54C8FF;
@lightGreen       : #2ECC40;
@lightOrange      : #FF851B;
@lightPink        : #FF8EDF;
@lightPurple      : #CDC6FF;
@lightRed         : #FF695E;
@lightTeal        : #6DFFFF;
@lightYellow      : #FFE21F;

@primaryColor     : @blue;
@secondaryColor   : @black;


/*-------------------
        Page
--------------------*/

@bodyBackground      : #FCFCFC;
@fontSize            : 14px;
@textColor           : rgba(0, 0, 0, 0.8);

@headerMargin        : 1em 0em 1rem;
@paragraphMargin     : 0em 0em 1em;

@linkColor           : #009FDA;
@linkUnderline       : none;
@linkHoverColor      : lighten( @linkColor, 5);
@linkHoverUnderline  : @linkUnderline;

@highlightBackground : #FFFFCC;
@highlightColor      : @textColor;



/*-------------------
  Background Colors
--------------------*/

@subtleTransparentBlack : rgba(0, 0, 0, 0.03);
@transparentBlack       : rgba(0, 0, 0, 0.05);
@strongTransparentBlack : rgba(0, 0, 0, 0.10);

@subtleTransparentWhite : rgba(255, 255, 255, 0.01);
@transparentWhite       : rgba(255, 255, 255, 0.05);
@strongTransparentWhite : rgba(255, 255, 255, 0.01);

/* Used for differentiating neutrals */
@subtleGradient: linear-gradient(transparent, rgba(0, 0, 0, 0.05));

/* Used for differentiating layers */
@subtleShadow: 0px 1px 2px 0 rgba(0, 0, 0, 0.05);


/*-------------------
        Grid
--------------------*/

@columnCount: 16;

/*-------------------
     Breakpoints
--------------------*/

@mobileBreakpoint            : 320px;
@tabletBreakpoint            : 768px;
@computerBreakpoint          : 992px;
@largeMonitorBreakpoint      : 1400px;
@widescreenMonitorBreakpoint : 1900px;

```



