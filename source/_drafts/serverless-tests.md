---
title: Testing Serverless
date: 2018-09-03 09:02:59
tags: ['Javascript', 'Serverless']
category: ['Javascript', 'Serverless', 'Node']
---


```js
let {expect} = require('chai')
let handler  = require('../handler')

require('isomorphic-fetch');
let fetchMock = require('fetch-mock');

describe('A placeholder test', ()=>{
    it('always passes', ()=>{
        expect(true).to.be.true
    })
})

describe('handler', ()=>{
    it('runs hello', async ()=>{
        let response = await handler.hello()
        expect(response.statusCode).to.equal(200)
        let {message} = JSON.parse(response.body)
        expect(message).to.equal('Go Serverless v1.0! Your function executed successfully!')
    })
})

describe('test asyncronously', () => {

    before( ()=>fetchMock.mock('http://patternmakerusa.com/hello', {
            status: 200,
            body: "Hello"
        })
        // transport streams on castus unit
        .mock(`http://patternmakerusa.com/world`, {status: 200, body: "World"})
    )

    after( ()=>fetchMock.reset() )

    it('Sucessfully returns Hello', async ()=>{
        let response = await fetch('http://patternmakerusa.com/hello')
        expect(response.status).to.equal(200)
        expect(await response.text() ).to.equal('Hello')
    })

    it('Sucessfully returns Hello and World', async ()=>{
        let response = await fetch('http://patternmakerusa.com/hello')
        expect(  await response.text() ).to.equal('Hello')
        response = await fetch('http://patternmakerusa.com/world')
        expect(  await response.text() ).to.equal('World')
    })

})
```

- [Serverless Test Example](https://github.com/didil/serverless-testing-examples)
- [Serverless Test Strategies](https://medium.com/@didil/serverless-testing-strategies-393bffb0eef8)
- [Mitm: for intercepting other ports](https://www.npmjs.com/package/mitm)

