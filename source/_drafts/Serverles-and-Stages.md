---
title: Serverles and Stages
tags: ['javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---


```json
  "scripts": {
    "deploy:production": "serverless deploy --verbose --stage production",
    "deploy:staging": "serverless deploy --verbose --stage staging",
  }
```
package.json


```yaml
provider:
  environment:
    JWT_SECRET: "PatternMaker_Software_Secret"
    MONGO_URL: ${self:custom.myEnvironment.MONGO_URL.${self:custom.myStage}}

custom:
  myStage: ${opt:stage, self:provider.stage} # myStage =  --stage CLI option || provider.stage
  myEnvironment:
    ENV_MESSAGE: # Must match var in provider
      staging: "This is staging environment"
      production: "This is production environment"
    MONGO_URL:
      staging: '<staging url>'
      production: '<production url>'
```
serverless.yaml

```javascript
let mongo_url  = process.env.MONGO_URL
```
any_controller.js
