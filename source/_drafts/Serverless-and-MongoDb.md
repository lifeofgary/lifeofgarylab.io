---
title: Serverless and MongoDb
tags: ['javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

```bash
yarn install mongoose
```

```yaml
provider:
  environment:
    MONGO_URL: ${self:custom.myEnvironment.MONGO_URL.${self:custom.myStage}}
custom:
  myStage: ${opt:stage, self:provider.stage} # myStage =  --stage CLI option || provider.stage
  myEnvironment:
    MONGO_URL:
      staging: 'mongodb://<username>:<password>@<staging>.mlab.com:55347/<staging_db_name>'
      production: 'mongodb://<username>:<password>@<production>.mlab.com:55347/<production_db_name>'
```
serverless.yml

```javascript
let mongoose = require('mongoose');

// MONGO_URL is set in serverless.yml
let mongo_url  = process.env.MONGO_URL
mongoose.Promise = Promise;

let connect = () => mongoose.connect( mongo_url, { useMongoClient: true } )
let disconnect = ()=>mongoose.disconnect()



if (process.env.NODE_ENV=="development")
  mongoose.set('debug', true)

module.exports.connect    = connect
module.exports.disconnect = disconnect
```
config.js

```javascript

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Types can include
// String
// Date
// Number
// Boolean

var schema = new Schema({
  test: String,
})

// https://stackoverflow.com/questions/14641834/how-to-get-rid-of-error-overwritemodelerror-cannot-overwrite-undefined-mode
let model
let modelName = "Test"
if (mongoose.models[modelName])
  model = mongoose.model(modelName)
else
  model = mongoose.model(modelName, schema)

// Export model
module.exports = model
```
model/test.js

```javascript
let {connect, disconnect} = require('config.js')
let TestModel     = require('model/test.js');

module.exports.order_validate_price = async (event, context, callback) => {
  console.log('order_validate_price called')
  let {id} = event

  await connect()

  let s = new Store({
    test: id,
  })
  await s.save()

  let store = await Store.findOne({test: id})

  console.log(store)

  await disconnect()


  console.log(store)
  callback(null, event);
}
```

## Tests

Monky can be used to create factories

# References

- https://mongoosejs.com/docs/index.html
- https://github.com/behrendtio/monky
