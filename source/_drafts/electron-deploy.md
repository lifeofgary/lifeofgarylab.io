---
title: electron-deploy
tags:
---

[Electron Builder](https://www.electron.build/)
[Electron Packager](https://github.com/electron-userland/electron-packager)

- [Walk Through](http://electron.rocks/electron-angular-packing-and-distribution/)
- [Distributing Electron Apps/](http://electron.rocks/distributing-electron-apps/)

- [Examples linkes](https://www.queryxchange.com/q/27_37113815/electron-builder-vs-electron-packager/)
- [Electron is MS store](https://blogs.msdn.microsoft.com/appconsult/2017/03/14/convert-your-electron-app-using-the-desktop-bridge/)
- [Node windows](https://www.npmjs.com/package/node-windows) links to [node-mac](https://github.com/coreybutler/node-mac) and [node-linux](https://github.com/coreybutler/node-linux)



