---
title: Serverles and Authorization
tags: ['Javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

# Endpoints

```yaml
provider:
  environment:
    JWT_SECRET: "Secret"
functions:
  authorize:
    handler: authorize.validate_token
  login:
    handler: login.create_token
    events:
      - http:
          path: login
          method: post
          cors: true
  store_index:
    handler: test.index
    events:
      - http:
          path: test
          method: get
          cors: true
          authorizer: authorize
```

# Login

```javascript
const jwt = require('jsonwebtoken');

let {login} = require('./authorize.js')
const JWT_EXPIRATION_TIME = '5d';

/**
  *
  * Returns a JWT, given a username and password.
  * @method login
  * @param {String} event.body.username
  * @param {String} event.body.password
  * @throws Returns 401 if the user is not found or password is invalid.
  * @returns {Object} jwt that expires in 5 mins
  */
  module.exports.create_token = async (event, context, callback) => {

    const { username, password } = JSON.parse(event.body);
    let response

    try {
      await connect()
      let pair = await login(username, password)

      let {dealer, user} = pair
      let to_encode =  {
        user: user._id,
        dealer: dealer._id.toString(),
        role: user.role,
      }
      // Issue JWT
      const token = jwt.sign(to_encode, process.env.JWT_SECRET, { expiresIn: JWT_EXPIRATION_TIME });

      response = { // Success response
        statusCode: 200,
        headers: { 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify({
          token,
          user: {
            username: user._id,
            role: user.role,
            code: user.code,
            format: user.format,
          },
        }),
      };

      await disconnect()
    } catch (e) {
      console.log(`Error logging in: ${e.message}`);
      response = { // Error response
        statusCode: 401,
        headers: { 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify({ error: e.message }),
      };

    }

    callback(null, response);
  };
```
login.js

# Authorize

```javascript
const jwt = require('jsonwebtoken');

let {connect, disconnect} = require('../../config/mongo_db.js')
const JWT_EXPIRATION_TIME = '5d';

// Returns a boolean whether or not a user is allowed to call a particular method
// A user with scopes: ['pangolins'] can
// call 'arn:aws:execute-api:ap-southeast-1::random-api-id/dev/GET/pangolins'
const authorizeUser = (role, methodArn) => {
  // TODO add permissions here
  return true
};

/**
  * Returns an IAM policy document for a given user and resource.
  *
  * @method buildIAMPolicy
  * @param {String} userId - user id
  * @param {String} effect  - Allow / Deny
  * @param {String} resource - resource ARN
  * @param {String} context - response context
  * @returns {Object} policyDocument
  */
  const buildIAMPolicy = (userId, effect, resource, context) => {
    const policy = {
      principalId: userId,
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: effect,
            Resource: resource,
          },
        ],
      },
      context,
    };
    return policy;
  };

/**
  * Returns a user, given a username and valid password.
  *
  * @method login
  * @param {String} username - user id
  * @param {String} password  - Allow / Deny
  * @throws Will throw an error if a user is not found or if the password is wrong.
  * @returns {Object} user
  */
let login = async (email, password) => {
  let dealer = await Dealer.findOne({'users.email': email})
  let user
  // find the user in dealer
  for (let u of dealer.users) {
    if (u.email==email)
      user = u
  }

  if (!user) throw new Error('User not found!');

  const hasValidPassword = (decode(user.password).user === password);
  if (!hasValidPassword) throw new Error('Invalid password');

  return {dealer, user};
};

/**
  * Authorizer functions are executed before your actual functions.
  * @method validate_token
  * @param {String} event.authorizationToken - JWT
  * @throws Returns 401 if the token is invalid or has expired.
  * @throws Returns 403 if the token does not have sufficient permissions.
  */
  module.exports.validate_token = (event, context, callback) => {
    const token = event.authorizationToken;
    try {
      // Verify JWT
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // const decoded = {user: "Admin", dealer: "5a52533b7114d35bee73233c", role: "admin", iat: 1515543945, exp: 1515975945}
      // Checks if the user's role allow her to call the current endpoint ARN
      const {user, role} = decoded;
      const isAllowed = true //authorizeUser(role, event.methodArn);

      // Return an IAM policy document for the current endpoint
      const effect = isAllowed ? 'Allow' : 'Deny';
      const userId = user;
      const authorizerContext = decoded; // { user: JSON.stringify(user), abc: 'xyz' };
      const policyDocument = buildIAMPolicy(userId, effect, event.methodArn, authorizerContext);

      callback(null, policyDocument);
    } catch (e) {
      // callback('nauthorized'); // Return a 401 nauthorized response
      callback(`error on auth ${e}`); // Return a 401 nauthorized response
    }
  };
module.exports.get_from_auth = get_from_auth
module.exports.login = login
```
autoorize.js



# References

https://github.com/yosriady/serverless-auth
