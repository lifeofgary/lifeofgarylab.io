---
title: 'Serverless: New Project'
tags: ['Javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

At the commmand line
```bash
# Create the project
npx serverless create --template aws-nodejs --path serverless-blog
cd serverless-blog
# Create a git reportitory
git init
# Make this a npm package
yarn init
yarn
# Add the serverless framework
yarn add serverless --dev
# Allow running serverless locally
npx serverless plugin install --name serverless-offline
# Add jsdoc for documentation
yarn add jsdoc --dev
# Add testing libraries
yarn add mocha  --dev
yarn add chai  --dev
yarn add fetch-mock
yarn add isomorphic-fetch
```


Add to `package.json`
```json
  "scripts": {
    "start": "sls offline start",
    "test": "env NODE_TLS_REJECT_UNAUTHORIZED=0 NODE_ENV=testing mocha --recursive test",
    "doc": "jsdoc src -r",
    "doc:view": "firefox out/index.html"
  },
```

Verify serverless offline works
```bash
yarn start
```

In the browser visit `http://localhost:3000/`  It should display
```
statusCode	404
error	"Serverless-offline: route not found."
currentRoute	"get - /"
existingRoutes	[]
```

Stop the server and add two lines to functions in `serverless.yml` to look like the following:
```yaml
functions:
  hello:
    handler: handler.hello
    events:
      - http: 'GET /hello'
```

* `hello` is the name of the aws function.  On aws it is referred to by this name
* `handler: handler.hello` attaches the aws to the file `handler.js` and the function `hello` in it.
* `http: 'GET /hello'` attaches the hello function to `localhost:3000/hello`

Note handler.js#hello was created automatically with the project.

A better format for the endpoint is
```javascript
functions:
  hello:
    handler: handler.hello
    events:
      - http:
          path: hello
          method: get
          cors: true
```
because cors can be added, which will be needed.


# References

- [Serverless Quickstart](https://serverless.com/framework/docs/providers/aws/guide/quick-start/)
- [Setting up Credentials](https://serverless.com/framework/docs/providers/aws/guide/credentials/)
- [Setting up Credentials: Video](https://www.youtube.com/watch?v=KngM5bfpttA)
- [Foo Bar Serverless playlist: Video](https://www.youtube.com/playlist?list=PLGyRwGktEFqe6stkPEFj4pBsDu0JMENxO)
- [Serverless Ci/Cd Tutorial](http://www.1strategy.com/blog/2018/02/27/serverless-cicd-tutorial-part-1-build/)
