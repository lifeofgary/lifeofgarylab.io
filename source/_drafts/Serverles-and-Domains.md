---
title: Serverles and Domains
tags: ['Javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

```yaml
plugins:
  - serverless-domain-manager
custom:
  domain:
    staging: 'staging.patternmaker.cc'
    production: 'reg.patternmaker.cc'
  customDomain:
    domainName:  ${self:custom.domain.${opt:stage}} # 'reg.patternmaker.cc'
    basePath: 'api' # This will be prefixed to all routes
    stage: ${opt:stage} # ${self:provider.stage}
    createRoute53Record: true
```
serverless.yml

```json
  "scripts": {
    "create_domain:staging": "serverless create_domain --stage staging",
    "create_domain:production": "serverless create_domain --stage production"
  }
```
package.json

References: [Api Gateway](https://serverless.com/blog/serverless-api-gateway-domain/)
[Domnain Manager Plugin](https://github.com/amplify-education/serverless-domain-manager)
