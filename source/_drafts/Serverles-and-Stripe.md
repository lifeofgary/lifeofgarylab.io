---
title: Serverles and Stripe
tags: ['Javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

```bash
yarn add stripe
```

```yaml
provider:
  environment:
    STRIPE_TOKEN: ${self:custom.myEnvironment.STRIPE_TOKEN.${self:custom.myStage}}
custom:
  myStage: ${opt:stage, self:provider.stage} # myStage =  --stage CLI option || provider.stage
  myEnvironment:
    STRIPE_TOKEN:
      staging: 'sk_test_xyz'
      production: 'sk_live_xyz'

```
`sk_test_xyz` and `sk_live_xyz` need to be gotten from stripe.com

```javascript
const stripe_token = process.env.STRIPE_TOKEN
let Stripe = require("stripe")(stripe_token);

module.exports.stripe = async (event, context, callback) => {
  console.log('stripe called')

  // This values come from the front end caller
  let {card, email, total} =  event
  let name = "PatternMaker User"

  let stripe_customer = await Stripe.customers.create({
    email: email,
    source: card
  })
  let charge = await Stripe.charges.create({
    amount: total,
    description: "PatternMaker Order",
    currency: "usd",
    customer: stripe_customer.id
  })
  callback(null, event);
};
```
handler.js

