---
title: Serverless and MailGun
tags: ['Javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

```bash
yarn add mailgun-js unescape
```

Get credemtails from  `https://app.mailgun.com/app/domains/mailgun_domain`
- mailgun_api_key
- mailgun_domain

```javascript
let unescape = require('unescape') // Convert Women&#39;s Volume => Women's 1

// from https://app.mailgun.com/app/domains/mg.patternmaker.info

// import the mailgun package
const mailgun = require('mailgun-js')({
  apiKey: <mailgun_api_key>,
  domain: <mailgun_domain>,
})

let send_mail = ({
  to,
  from = 'User <user@domain>',
  subject,
  text
})=>{
  // The to should be extracted from event
  const mailData = {
    from,
    to,
    subject: unescape(subject),
    text: unescape(text),
  }
  return new Promise( (resolve, reject) => {
    mailgun.messages().send(mailData, (sendError, body) => {
    // Send error is undefined unless there is an error
    if (sendError)
      reject(sendError)
    else
      resolve()
    })
  })
}
module.exports.send_mail = send_mail

module.exports.send =  (event, context, callback) => {
  let {to, from, subject, text} = JSON.parse(event)
  let response

  send_mail({to, from, subject, text}).then( result=>{

  })
}
```

# References

- https://github.com/bojand/mailgun-js


