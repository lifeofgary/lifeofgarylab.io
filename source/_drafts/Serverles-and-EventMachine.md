---
title: Serverles and EventMachine
tags: ['javascript', 'Serverless', 'Node']
category: ['Javascript', 'Serverless', 'Node']
---

```yaml
provider:
  - Effect: "Allow"
    Action:
      - "states:ListStateMachines"
      - "states:StartExecution"
    Resource: "arn:aws:states:*:*:*"
functions:
  machine_start:
    handler: handler.machine_start
    events:
      - http:
          path: start_machine
          method: post
  machine_step_one:
    handler:  handler.machine_step_one
  machine_step_two:
    handler:  hanler.machine_step_two
  language_create:
    handler: controller/language/language.create
    events:
      - http:
          path: languages
          method: post

stepFunctions:
  stateMachines:
    orderStateMachine:
      name: TestStateMachine
      definition:
        Comment: "A test state machine"
        StartAt: machine_step_one
        States:
          machine_step_one:
            Type: Task
            Resource: arn:aws:lambda:#{AWS::Region}:#{AWS::AccountId}:function:${self:service}-${opt:stage}-machine_step_one
            Next: machine_step_two
          machine_step_two:
            Type: Task
            Resource: arn:aws:lambda:#{AWS::Region}:#{AWS::AccountId}:function:${self:service}-${opt:stage}-machine_step_two
            End: true
plugins:
  - serverless-step-functions

```
serverless.yml

Notes:
- Each arn must be unique.  Append the -state_name
- each state name matches a function name

Create a utility for finding a starting a step function

```javascript
const AWS = require('aws-sdk');
const stepfunctions = new AWS.StepFunctions();

module.exports.callStepFunction = (stateMachineName, stateMachineData) => {
  return stepfunctions
    .listStateMachines({})
    .promise()
    .then(listStateMachines => {
      // Loop through the list of state machines
      for (var i = 0; i < listStateMachines.stateMachines.length; i++) {
        const item = listStateMachines.stateMachines[i];

        // Did we find the right state machine
        if (item.name.indexOf(stateMachineName) >= 0) {
          // set the call values
          var params = {
            stateMachineArn: item.stateMachineArn,
            input: JSON.stringify(stateMachineData)
          };

          // Call the state machine
          return stepfunctions.startExecution(params).promise().then(() => {
            console.log(`Running ${stateMachineName}`)
            return true;
          });
        }
      }
    })
    .catch(error => {
      console.log("problem searching step functions")
      console.log(`Could not call ${stateMachineName} with ${JSON.stringify(stateMachineData)}`);
      console.log(error)
      console.log(JSON.stringify(error))
      return false;
    });
}
```
util.js

Create the state machine functions
```javascript
let {callStepFunction} = require('./util')

module.exports.machine_start = async (event, context, callback) => {
  // Post data to pass to the machine
  let data = JSON.parse(event.body)

  // The first param is the name of the machine, which must match the yamo
  callStepFunction('TestStateMachine', data).then( () => {
    let message = 'Test is starting the machine';

    const response = {
      statusCode: 200,
      body: JSON.stringify({ data: message }),
      headers: {
        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work\
      }
    };

    return callback(null, response);
  });
}

module.exports.machine_step_one = async (event, context, callback) => {
  console.log(`machine_step_one called with ${event}`)
  event.more = 'data'
  // Forward the event data to the next step
  callback(null, event);
};

module.exports.machine_step_two = async (event, context, callback) => {
  console.log(`machine_step_two called with ${event}`)
  callback(null, event);
};

```
handler.js

# References

[Serverless Blog](https://serverless.com/blog/how-to-manage-your-aws-step-functions-with-serverless/)
[Foo Bar on Youtube](https://www.youtube.com/watch?v=9MKL5Jr2zZ4&list=PLGyRwGktEFqd_YBnm5Zxzw9GP1OnEFO_U) 4 videos


